const express = require("express");
const bodyParser = require("body-parser");
const pg = require("pg");
const path = require("path");  // Agrega esta línea

const app = express();
const port = 3000;

// Configuración de la base de datos
const pool = new pg.Pool({
  user: "crudjs",
  host: "localhost",
  database: "crudjs",
  password: "1234",
  port: 5432
});

app.use(bodyParser.json());

app.use('/static', express.static('static'));

// Ruta para servir el archivo index.html
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "index.html"));
  });

// Ruta para obtener todos los items
app.get("/items", async (req, res) => {
  try {
    const client = await pool.connect();
    const result = await client.query("SELECT * FROM items");
    const items = result.rows;
    client.release();

    res.json(items);
  } catch (error) {
    console.error("Error al obtener los items:", error);
    res.status(500).send("Error al obtener los items");
  }
});

// Ruta para crear un nuevo item
app.post("/items", async (req, res) => {
  const { nombre, descripcion, precio } = req.body;

  try {
    const client = await pool.connect();
    await client.query("INSERT INTO items(nombre, descripcion, precio) VALUES($1, $2, $3)", [nombre, descripcion, precio]);
    client.release();

    res.status(201).send("Item creado exitosamente");
  } catch (error) {
    console.error("Error al crear el item:", error);
    res.status(500).send("Error al crear el item");
  }
});

app.listen(port, () => {
  console.log(`Servidor en ejecución en http://localhost:${port}`);
});
