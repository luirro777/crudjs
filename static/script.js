const listaItems = document.getElementById("listaItems");

async function getItems() {
  try {
    const response = await fetch("/items");
    const data = await response.json();

    listaItems.innerHTML = "";
    data.forEach(item => {
      const li = document.createElement("li");
      li.innerHTML = `${item.nombre} - ${item.descripcion} - $${item.precio}`;
      listaItems.appendChild(li);
    });
  } catch (error) {
    console.error("Error al obtener los items:", error);
  }
}

async function crearItem() {
  const nombre = document.getElementById("nombre").value;
  const descripcion = document.getElementById("descripcion").value;
  const precio = parseFloat(document.getElementById("precio").value);

  const newItem = { nombre, descripcion, precio };

  try {
    await fetch("/items", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(newItem)
    });

    getItems();
  } catch (error) {
    console.error("Error al crear el item:", error);
  }
}

getItems();
